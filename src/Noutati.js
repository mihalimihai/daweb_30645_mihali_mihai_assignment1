import React, {Component} from 'react';
import Background from './back.jpg';
import ReactPlayer from "react-player";

let sectionStyle = {
    width: "64%",
    height: "400px",
    backgroundImage: "url(" + Background + ")"
};

class Noutati extends Component{
    render() {
        return (
            <div className="nou">
                    <section style={sectionStyle}>
                        <div>
                            <h3 className="m-3 d-flex justify-content-center">5 exemple de boli de ochi pe care aplicatia le poate detecta:</h3>
                            <ReactPlayer url="https://www.youtube.com/watch?v=xKScC5OHBgw" controls={true}/>
                        </div>
                    </section>
                <h4>Aceasta aplicatie este foarte utila pentru diferite categorii de persoane, de la varstnici pana la copii, de la
                    corporatisti pana la gunoieri.
                    Ajuta pentru detectia bolilor la ochi, si pentru testarea vederii, atat periferica cat si cea centrala.
                    Este foarte usor de utilizat, iar precizia raspunsului dat de aplicatie este una mare, astfel oftalmologii pot sa
                    emita un dignostic precis pentru pacientul respectiv. Urmand ca pacientul sa se poata trata corespunzator bolii pe care o are.</h4>

            </div>
        )
    }
}
export default Noutati;