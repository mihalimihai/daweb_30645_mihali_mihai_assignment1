import React, {Component} from 'react';
import UserProfile from "react-user-profile"
import Prof from './prof.jpg';

class Coordonator extends Component {
    render() {
        const photo = {Prof}
        const userName = 'Sebestyen Gheorghe'
        const email = 'gheorghe.sebestyen@cs.utcluj.ro'

        const comments = [
            {
                id: '1',
                userName: '',
                content: 'Un profesor deosebit',
                createdAt: 1543858000000
            }
        ]

        return (
            <div style={{margin: '0 auto', width: '100%'}}>
                <UserProfile photo={Prof} userName={userName} email={email}  location={email}/>
            </div>
        )
    }
}
export default Coordonator;