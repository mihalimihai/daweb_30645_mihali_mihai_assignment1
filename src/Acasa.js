import React, {Component} from 'react';
import P1 from './p1.jpg';
import ReactPlayer from 'react-player';

class Acasa extends Component{
    render() {
        return (
            <div className="container">
                <h3 className="m-3 d-flex justify-content-center" >Medical application for the detection of eye diseases</h3>
                <img src={P1} />
                <h4 className="m-3 d-flex justify-content-center" >Arduino video presentation:</h4>
                <ReactPlayer url="https://www.youtube.com/watch?v=_ItSHuIJAJ8" controls={true}
                />
            </div>
        )
    }
}
export default Acasa;