import React, {Component} from 'react';
import Background from './back.jpg';
import P2 from './p2.jpg';
import P3 from './p3.jpg';

let sectionStyle = {
    width: "100%",
    height: "400px",
    backgroundImage: "url(" + Background + ")"
};

class DespreLucrare extends Component{
    render() {
        return (
            <div className="nou">

                <section style={sectionStyle}>
                    <div>
                        <h3>
                            Arduino este o companie de hardware și software open source, comunitate de proiecte și utilizatori
                            care proiectează și produce microcontrolere cu un singur tablou și kituri de microcontrolere pentru construirea dispozitivelor digitale.
                            Proiectul meu de diploma, consta dintr-o macheta in forma de semi-sfera, facuta din ghips. In interiorul acestei machete
                            se afla undeva la un numar de 80 de LED-uri, pe care user-ul trebuie sa apese pe un buton daca vede LED-ul. Toate aceste componente
                            sunt conectate la placuta Arduino UNO, ulterior va fi inlocuita cu una MEGA pentru mai multe functionalitati, care la randul ei este conectata la calculator.
                            Un program scris in Java primeste datele de la Arduino si deseneaza un Canvas cu rezultatul obtinut in urma examinarii pacientului,
                            iar dupa acest desen, medicul specialist poate sa emita un diagnostic.
                        </h3>
                        <img src={P2}/>
                        <img src={P3}/>
                    </div>

                </section>

            </div>
        )
    }
}
export default DespreLucrare;