import React, {Component} from 'react';
import UserProfile from "react-user-profile"
import Poza from './poza.jpg';

class Student extends Component {
    render() {
        const photo = {Poza}
        const userName = 'Mihali Ioan-Mihai'
        const email = '.Net Java Arduino ' +
            'mihalimihai97@gmail.com'
        const interese = '.Net Java Arduino'

        const comments = [
            {
                id: '1',
                userName: 'Random Dude',
                content: 'Un student deosebit',
                createdAt: 1543858000000
            }
        ]

        return (
            <div style={{margin: '0 auto', width: '100%'}}>
                <UserProfile photo={Poza} userName={userName} location={email}/>
            </div>
        )
    }
}
export default Student;