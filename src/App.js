import React, { Component } from 'react';
import Menu from './Menu';
import logo from './arduino.png';
import Student from "./Student";
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import Coordonator from "./Coordonator";
import Acasa from "./Acasa";
import Contact from "./Contact";
import Noutati from "./Noutati";
import DespreLucrare from "./DespreLucrare";

class App extends Component {
  render() {
    let links = [
      { label: 'Acasa', link: '/acasa', active: true },
      { label: 'Noutati', link: '/noutati' },
      { label: 'Despre lucrare', link: '/despre' },
      { label: 'Student', link: '/student'},
      { label: 'Coordonator', link: '/coordonator' },
      { label: 'Contact', link: '/contact' },
    ];

    return (
      <div className="container center">
        <Router>
          <div>
            <Menu links={links} logo={logo} />

            <Switch>
               <Route
               exact
               path='/student'
               render={()=><Student/>}
               />

               <Route
                   exact
                   path='/coordonator'
                   render={()=><Coordonator/>}
                   />

                <Route
                    exact
                    path='/acasa'
                    render={()=><Acasa/>}
                />

                <Route
                    exact
                    path='/contact'
                    render={()=><Contact/>}
                />

                <Route
                    exact
                    path='/noutati'
                    render={()=><Noutati/>}
                />

                <Route
                    exact
                    path='/despre'
                    render={()=><DespreLucrare/>}
                />

             </Switch>

           </div>
        </Router>
      </div>
    );
  }
}

export default App;
